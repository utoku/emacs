;; circadian. theme support with time. 
(defconst YF/latitude  38.73) ;; N
(defconst YF/longitude 26.83) ;; E

(defconst YK/latitude  32.1)  ;; N
(defconst YK/longitude 27.29) ;; E

(defconst SF/latitude  37.77)   ;; N
(defconst SF/longitude -122.41) ;; W



(use-package circadian
  :config
  (setq calendar-latitude  YF/latitude)
  (setq calendar-longitude YF/longitude)
  (setq circadian-themes '((:sunrise . hydandata-light)
                           (:sunset . sanityinc-tomorrow-eighties)))
  (circadian-setup)
  ;;enable in case we are using desktop 
  :hook (desktop-after-read . circadian-setup )
  )
