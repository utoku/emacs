;; Some functions to make theme switching easier



(defvar *leod-theme-dark* 'sanityinc-tomorrow-eighties)
(defvar *leod-theme-gray* 'zenburn)
(defvar *leod-theme-light* 'hydandata-light)
(defvar *leod-current-theme* *leod-theme-dark*) ;open up with the dark one

;; disable other themes before loading new one
(defadvice load-theme (before theme-dont-propagate activate)
  "Disable theme before loading new one."
  (mapcar #'disable-theme custom-enabled-themes))


(defun leod/next-theme (theme)
  (if (eq theme 'default)
      (disable-theme *leod-current-theme*)
    (progn
      (load-theme theme t)))
  (setq *leod-current-theme* theme))


;;bind this to a key or something to switch between themes.
(defun leod/toggle-theme ()
  (interactive)
  (cond ((eq *leod-current-theme* *leod-theme-dark*) (leod/next-theme *leod-theme-light*))
        ((eq *leod-current-theme* *leod-theme-light*) (leod/next-theme *leod-theme-gray*))
        ((eq *leod-current-theme* *leod-theme-gray*) (leod/next-theme *leod-theme-dark*))
        ((eq *leod-current-theme* 'default) (leod/next-theme *leod-theme-dark*))))
