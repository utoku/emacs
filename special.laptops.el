;;special stuff for laptops, where functions keys are hard to use.

(global-set-key [ (control b) ] 'buffer-menu)
(global-set-key [ (control .) ] 'kill-this-buffer)
;(global-set-key [ (control 0) ] 'kill-this-buffer) ;;cant override 


;(setq visible-bell 1)
